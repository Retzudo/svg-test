from flask import Flask, request, render_template, Response

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/usa.svg")
def usa_svg():
    states = request.args.getlist("highlight")
    svg = render_template("usa.svg", states=states)

    return Response(svg, mimetype="image/svg+xml")
